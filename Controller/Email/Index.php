<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Avanti\SellerEmailToQuote\Controller\Email;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;

use Magento\Checkout\Model\Cart;

class Index extends Action
{
    protected $resultPageFactory;
    protected $cart;

    /**
     * Constructor
     * @param Context  $context
     * @param PageFactory $resultPageFactory
     * @param Cart $cart
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Cart $cart
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->cart  = $cart;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        try {
            $quote = $this->cart->getQuote();
            $email = $this->getRequest()->getParam('seller_email') ?? null;
            $quote->setData('seller_email', $email);
            $quote->save();
        } catch (\Exception $e) {
            return $this->resultFactory
                ->create(ResultFactory::TYPE_JSON)
                ->setData([
                    'success' => false,
                    'message' => __('Error:' + $e)
                ]);
        }

        return $this->resultFactory
            ->create(ResultFactory::TYPE_JSON)
            ->setData([
                'success' => true,
                'message' => __('Email set with success')
            ]);
    }
}