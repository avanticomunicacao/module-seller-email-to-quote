<?php
namespace Avanti\SellerEmailToQuote\Ui\Component\Listing\Collumns;

use Magento\Ui\Component\Listing\Columns\Column;

class SellerEmail extends Column
{
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $sellerEmail = isset($item['seller_email']) ? $item['seller_email'] : null;
                if (!$sellerEmail) {
                    $item['seller_email'] = __('E-mail not Informed');
                }
            }
        }
        return $dataSource;
    }
}