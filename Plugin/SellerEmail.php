<?php
namespace Avanti\SellerEmailToQuote\Plugin;

use Magento\Sales\Api\Data\OrderExtensionFactory;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderSearchResultInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Checkout\Model\Cart;

class SellerEmail
{
    /**
     * Order Extension Attributes Factory
     *
     * @var OrderExtensionFactory
     */
    protected $orderExtensionFactory;

    /**
     * @var Cart
     */
    protected $cart;

    /**
     * SellerEmail constructor.
     * @param OrderExtensionFactory $orderExtensionFactory
     * @param Cart $cart
     */
    public function __construct(
        OrderExtensionFactory $orderExtensionFactory,
        Cart $cart)
    {
        $this->orderExtensionFactory = $orderExtensionFactory;
        $this->cart = $cart;
    }

    /**
     * Insert seller email in order Get API
     * @param OrderRepositoryInterface $subject
     * @param OrderInterface $order
     * @return OrderInterface
     */
    public function afterGet(OrderRepositoryInterface $subject, OrderInterface $order)
    {
        $sellerEmail = $order->getSellerEmail();

        if($sellerEmail != null) {
            $extensionAttributes = $order->getExtensionAttributes();
            $extensionAttributes = $extensionAttributes ? $extensionAttributes : $this->orderExtensionFactory->create();
            $extensionAttributes->setSellerEmail($sellerEmail);
            $order->setExtensionAttributes($extensionAttributes);
        }

        return $order;
    }

    /**
     * Insert seller email in all order list
     * @param OrderRepositoryInterface $subject
     * @param OrderSearchResultInterface $searchResult
     * @return OrderSearchResultInterface
     */
    public function afterGetList(OrderRepositoryInterface $subject, OrderSearchResultInterface $searchResult)
    {
        $orders = $searchResult->getItems();
        foreach ($orders as &$order) {
            $sellerEmail = $order->getSellerEmail();
            $extensionAttributes = $order->getExtensionAttributes();
            $extensionAttributes = $extensionAttributes ? $extensionAttributes : $this->orderExtensionFactory->create();
            $extensionAttributes->setSellerEmail($sellerEmail);
            $order->setExtensionAttributes($extensionAttributes);
        }

        return $searchResult;
    }

    /**
     * Insert seller email in order Get API
     * @param OrderRepositoryInterface $subject
     * @param OrderInterface $order
     * @return OrderInterface
     */
    public function afterSave(OrderRepositoryInterface $subject, OrderInterface $order)
    {
        $quote = $this->cart->getQuote();

        $order->setSellerEmail($quote->getSellerEmail());
        $order->save();

        return $order;
    }
}
