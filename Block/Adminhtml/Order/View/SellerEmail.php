<?php


namespace Avanti\SellerEmailToQuote\Block\Adminhtml\Order\View;

use Magento\Backend\Block\Template;
use Magento\Framework\Exception\LocalizedException as LocalizedExceptionAlias;
use Magento\Framework\Registry;
use Magento\Sales\Model\Order;

class SellerEmail extends Template
{
    /**
     * Core registry
     *
     * @var Registry
     */
    protected $coreRegistry = null;

    public function __construct(
        Template\Context $context,
        Registry $registry,
        array $data = [])
    {
        $this->coreRegistry  = $registry;
        parent::__construct($context, $data);
    }

    /**
     * Retrieve available order
     *
     * @return Order
     * @throws LocalizedExceptionAlias
     */
    public function getOrder()
    {
        if ($this->hasOrder()) {
            return $this->getData('order');
        }
        if ($this->coreRegistry->registry('current_order')) {
            return $this->coreRegistry->registry('current_order');
        }
        if ($this->coreRegistry->registry('order')) {
            return $this->coreRegistry->registry('order');
        }
        throw new LocalizedExceptionAlias(__('We can\'t get the order instance right now.'));
    }
}